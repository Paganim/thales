import cv2
import numpy as np
import math
from PIL import Image
import numpy as np
import glob

#################################################
#################################################
## BILATERAL FILTER
#################################################
#################################################


def limit(x):
    x = 0 if x < 0 else x
    x = 255 if x > 255 else x
    return x

def generate_bilateral_filter_distance_matrix(window_length,sigma):
    distance_matrix = np.zeros((window_length,window_length,3))
    left_bias = int(math.floor(-(window_length - 1) / 2))
    right_bias = int(math.floor((window_length - 1) / 2))
    for i in range(left_bias,right_bias+1):
        for j in range(left_bias,right_bias+1):
            distance_matrix[i-left_bias][j-left_bias] = math.exp(-(i**2+j**2)/(2*(sigma**2)))
    return distance_matrix

def look_for_gaussion_table(delta):
        return delta_gaussion_dict[delta]

def bilateral_filter(image_matrix,contrast=25,space=9):
    global delta_gaussion_dict
    window_length = 7
    mask_image_matrix = np.zeros((image_matrix.shape[0], image_matrix.shape[1]))
    image_matrix = image_matrix.astype(np.int32)

    limit_ufun = np.vectorize(limit, otypes=[np.uint8])
    delta_gaussion_dict = {i: math.exp(-i ** 2 / (2 *(contrast**2))) for i in range(256)}
    look_for_gaussion_table_ufun = np.vectorize(look_for_gaussion_table, otypes=[np.float64])
    bilateral_filter_distance_matrix = generate_bilateral_filter_distance_matrix(window_length,space)

    margin = int(window_length / 2)
    left_bias = math.floor(-(window_length - 1) / 2)
    right_bias = math.floor((window_length - 1) / 2)
    filter_image_matrix = image_matrix.astype(np.float64)

    for i in range(0 + margin, image_matrix.shape[0] - margin):
        for j in range(0 + margin, image_matrix.shape[1] - margin):
            if mask_image_matrix[i][j]==0:
                filter_input = image_matrix[i + left_bias:i + right_bias + 1,
                               j + left_bias:j + right_bias + 1]
                bilateral_filter_value_matrix = look_for_gaussion_table_ufun(np.abs(filter_input-image_matrix[i][j]))
                bilateral_filter_matrix = np.multiply(bilateral_filter_value_matrix, bilateral_filter_distance_matrix)
                bilateral_filter_matrix = bilateral_filter_matrix/np.sum(bilateral_filter_matrix,keepdims=False,axis=(0,1))
                filter_output = np.sum(np.multiply(bilateral_filter_matrix,filter_input),axis=(0,1))
                filter_image_matrix[i][j] = filter_output
    filter_image_matrix = limit_ufun(filter_image_matrix)
    return filter_image_matrix


################################################
################################################
## ADD NOISE TO IMAGE
################################################
################################################


def gaussian_noise(x,mu,std):
    noise = np.random.normal(mu, std, size = x.shape)
    x_noisy = x + noise
    return x_noisy

##########################################################

def signaltonoise(a, axis=0, ddof=0):
    a = np.asanyarray(a)
    m = a.mean(axis)
    sd = a.std(axis=axis, ddof=ddof)
    return np.where(sd == 0, 0, m/sd)

if __name__ == "__main__":
    # global delta_gaussion_dict
    # delta_gaussion_dict = None
    # mu = 0.0

    # img = cv2.imread("../images/boat_test.png")

    # std_1 = 0.10 * np.std(img) # for 10% Gaussian noise
    # std_2 = 0.20 * np.std(img) # for 20% Gaussian noise
    # std_3 = 0.30 * np.std(img) # for 30% Gaussian noise

    # noised_1 = gaussian_noise(img, mu, std_1)
    # noised_2 = gaussian_noise(img, mu, std_2)
    # noised_3 = gaussian_noise(img, mu, std_3)

    # cv2.imwrite('../images/noised_1.png', noised_1)
    # cv2.imwrite('../images/noised_2.png', noised_2)
    # cv2.imwrite('../images/noised_3.png', noised_3)


    ## Begin of bilateral use
    # bilat_filter_img_1 = bilateral_filter(noised_1, contrast=20)
    # cv2.imwrite("../images/filtered_image_1.png", bilat_filter_img_1)

    # delta_gaussion_dict = None
    # bilat_filter_img_2 = bilateral_filter(noised_2)
    # cv2.imwrite("../images/filtered_image_2.png", bilat_filter_img_2)

    # delta_gaussion_dict = None
    # bilat_filter_img_3 = bilateral_filter(noised_3, contrast=40)
    # cv2.imwrite("../images/filtered_image_3.png", bilat_filter_img_3)

    # sum images
    sum_images = np.array(Image.open("../images/knee4_1344x1344_35frame_4.2nGy0000.tif"))
    for i in range(1, len(glob.glob("../images/*.tif"))):
        sum_images += np.array(Image.open(glob.glob("../images/*.tif")[i]))

    im = Image.fromarray(sum_images)
    im.save("../images/sum_images.tiff")

    # substract image
    img_ten = np.array(Image.open("../images/knee4_1344x1344_35frame_4.2nGy0010.tif"))
    substracted_image = Image.fromarray(img_ten - sum_images)
    substracted_image.save("../images/substracted_image.tiff")

    compute = signaltonoise(substracted_image, axis=None)
    print("SNR:", compute)
    print("Mean:", np.mean(substracted_image))
    print("Standard deviation:", np.std(substracted_image))
